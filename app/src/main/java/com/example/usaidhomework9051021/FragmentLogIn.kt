package com.example.usaidhomework9051021

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.usaidhomework9051021.databinding.FragmentLogInBinding
import com.google.firebase.auth.FirebaseAuth

class FragmentLogIn : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var binding:FragmentLogInBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLogInBinding.inflate(inflater, container, false)
        return binding.root
        init()
    }

    private fun init() {
        binding.logInLogInButton.setOnClickListener {
            signin()
        }
    }

    private fun signin() {
        val emaiET:String = binding.logInEmailET.text.toString()
        val passwordET:String = binding.logInPasswordET.text.toString()

        if (emaiET.isNotEmpty() && passwordET.isNotEmpty()){
            auth.signInWithEmailAndPassword(emaiET, passwordET)
                .addOnCompleteListener(requireActivity()) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("signIn", "signInWithEmail:success")
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(context, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        }else {
            Toast.makeText(requireContext(), "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }

}


