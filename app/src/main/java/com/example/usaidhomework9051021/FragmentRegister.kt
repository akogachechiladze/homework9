package com.example.usaidhomework9051021

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.usaidhomework9051021.databinding.FragmentInitBinding
import com.example.usaidhomework9051021.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class FragmentRegister : Fragment() {

    private lateinit var auth: FirebaseAuth

    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
        init()
    }

    private fun init() {
        auth = Firebase.auth
        binding.registerNextButton.setOnClickListener {
            signup()
        }
    }

    private fun signup() {
        val emailET:String = binding.registerEmailET.text.toString()
        val passwordET: String = binding.registerPasswordET.text.toString()

        if (emailET.isNotEmpty() && passwordET.isNotEmpty()){
            Log.d("SignUp", "createUserWithEmail:success")
            val user = auth.currentUser

        }else{
            Toast.makeText(requireContext(), "Please fill all fields", Toast.LENGTH_SHORT).show()
        }

    }

    private fun addListener() {
        binding.registerNextButton.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentRegister_to_fragmentRegisterTwo)
        }

    }

}



