package com.example.usaidhomework9051021

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.usaidhomework9051021.databinding.FragmentRegisterTwoBinding

class FragmentRegisterTwo : Fragment() {

    private lateinit var binding: FragmentRegisterTwoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterTwoBinding.inflate(inflater, container, false)
        return binding.root
        registrationfinish()
    }

    private fun registrationfinish() {
        val nickname: String = binding.registerTwoNicknameET.text.toString()
        if (nickname.isNotEmpty()) {
            Toast.makeText(requireContext(), "Registration finished", Toast.LENGTH_SHORT).show()
        }
    }

}