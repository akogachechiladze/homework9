package com.example.usaidhomework9051021

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.usaidhomework9051021.databinding.FragmentInitBinding


class FragmentInit : Fragment() {

    private lateinit var binding:FragmentInitBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInitBinding.inflate(inflater, container, false)
        setListeners()
        return binding.root

    }

    private fun setListeners() {

        binding.mainRegisterButton.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentInit_to_fragmentRegister)
        }

        binding.mainLogInButton.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentInit_to_fragmentLogIn)
        }

    }

}